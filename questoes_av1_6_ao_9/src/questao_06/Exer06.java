package questao_06;
import java.util.Scanner;

public class Exer06 {
	static Scanner tecla = new Scanner(System.in);
	
	public static void main(String[] args) {
		//INICIA OBJETOS E ADICIONA PRODUTOS AO ARRAY DE PRODUTOS
		Mochila mochila = new Mochila(20.6);
		Pessoa pessoa = new Pessoa(12.5, 10.2, mochila);
		Produto arroz = new Produto(0,5.5,5.0,"Arroz");
		Produto feijao = new Produto(1,6.5,1.0,"Feijão");
		Produto carneMoida = new Produto(2,11.99,0.5,"Carne Moída");
		Produto chocolate = new Produto(3,2.5,.3,"Barra de Chocolate");
		Produto[] arr_prod = new Produto[4];
		for(int i = 0; i <= arr_prod.length-1;i++) {
			switch (i) {
				case 0: arr_prod[i] = arroz; break;
				case 1: arr_prod[i] = feijao; break;
				case 2:	arr_prod[i] = carneMoida; break;
				case 3: arr_prod[i] = chocolate; break;
			}
		}
		//INICIA LOOP PRINCIPAL
		boolean end = false;
		int opt = 0;
		do {
			System.out.println("Defina uma opção:");
			System.out.println("1 - Ver produtos");
			System.out.println("2 - Ver mochila e limites");
			System.out.println("3 - Ver apenas os limites");
			System.out.println("4 - Adicionar produto na mochila");
			System.out.println("5 - Retirar produto da mochila");
			System.out.println("0 - Sair");
			opt = tecla.nextInt();
			switch (opt) {
				case 1: pessoa.verProdutos(arr_prod); break;
				case 2: pessoa.verMochila(); break;
				case 3: pessoa.verLimites(); break;
				case 4: pessoa.escolheProduto(arr_prod); break;
				case 5: pessoa.retiraProduto(); break;
				case 0: end = true; break;
				default: System.out.println("Opção inválida!");  break;
			}
		} while (!end);
		
		
	}
	
	//METODOS
	
	

}
