package questão_wpp_prova_doria;

public class CachePessoa {
	static private int[] cache = new int[5];
	static private int index = 0;
	
	public void retornaPessoa(Pessoa[] lista, int id) {
		boolean found = false;
		for(int i = 0; i <= cache.length-1; i++) {
			if(cache[i] != 0 && id == cache[i]) {
				System.out.printf("Nome: %s | Idade: %d\n", lista[i].getNome(), lista[i].getIdade());
				found = true;
				break;
			}
		}
		if(!found) {
			for(int i = 0; i <= lista.length-1; i++) {
				if(lista[i] != null && id == lista[i].getCod()) {
					System.out.printf("Correndo a lista - Nome: %s | Idade: %d\n", lista[i].getNome(), lista[i].getIdade());
					cache[index++] = id;
					break;
				}
			}
		}
	}
	

}
