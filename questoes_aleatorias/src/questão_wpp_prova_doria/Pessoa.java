package questão_wpp_prova_doria;

public class Pessoa {
	private int cod, idade;
	private String nome;
	
	public Pessoa(int cod, String nome, int idade) {
		this.cod = cod;
		this.nome = nome;
		this.idade = idade;
	}

	public int getCod() {
		return cod;
	}

	public void setCod(int cod) {
		this.cod = cod;
	}

	public int getIdade() {
		return idade;
	}

	public void setIdade(int idade) {
		this.idade = idade;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}
}
