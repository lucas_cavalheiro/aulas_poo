package questão_wpp_prova_doria;

public class Aplicativo {
	static Pessoa[] lista = new Pessoa[5];
	static Pessoa[] lista2 = new Pessoa[5];
	
	public static void main(String[] args) {
		lista[0] = new Pessoa(1,"João",10);
		lista[1] = new Pessoa(2,"Alice",5);
		lista[2] = new Pessoa(3,"Fernando",27);
		lista[3] = new Pessoa(4,"Carlos",12);
		lista[4] = new Pessoa(5,"Priscila",31);
		
		imprime();
		ordena();
		System.out.println("-----------------");
		imprime();
		removePorIdade(27);
		System.out.println("-----------------");
		imprime();
		
		lista2[0] = new Pessoa(1,"Paulo",8);
		lista2[1] = new Pessoa(2,"Silas",19);
		lista2[2] = new Pessoa(3,"Paulo",18);
		lista2[3] = new Pessoa(4,"Pedro",25);
		lista2[4] = new Pessoa(5,"Paulo",50);
		
		System.out.println("---------------");
		imprime(lista2);
		
		CachePessoa cache = new CachePessoa();
		System.out.println("-----------------------");
		cache.retornaPessoa(lista2, 1);
		cache.retornaPessoa(lista2, 2);
		cache.retornaPessoa(lista2, 2);
		cache.retornaPessoa(lista2, 3);
		System.out.println("=========================");
		cache.retornaPessoa(lista2, 1);
		cache.retornaPessoa(lista2, 2);
		cache.retornaPessoa(lista2, 2);
		cache.retornaPessoa(lista2, 3);
		
		
	}
	
	public static void imprime() {
		for(int i = 0; i <= lista.length-1;i++) {
			if(lista[i] != null) System.out.printf("Nome: %s | Cod: %d | Idade: %d\n", lista[i].getNome(), lista[i].getCod(), lista[i].getIdade());
		}
	}
	
	public static void imprime(Pessoa lista[]) {
		for(int i = 0; i <= lista.length-1;i++) {
			if(lista[i] != null && lista[i].getIdade() >= 21) {
				System.out.printf("Nome: %s | Cod: %d | Idade: %d\n", lista[i].getNome(), lista[i].getCod(), lista[i].getIdade());
			}
		}
	}
	
	public static void ordena() {
		Pessoa aux = null;
		for(int i = 0; i <= lista.length-1;i++) {
			for(int j = 0; j <= lista.length-1;j++) {
				if(lista[i] != null && lista[j] != null && lista[i].getIdade() < lista[j].getIdade()) {
					aux = lista[i];
					lista[i] = lista[j];
					lista[j] = aux;
				}
			}
		}
	}
	
	public static void removePorIdade(int chave) {
		for(int i = 0; i <= lista.length-1;i++) {
			if(lista[i] != null & chave == lista[i].getIdade()) {
				lista[i] = null;
				System.gc();
				break;
			}
		}
	}

}
