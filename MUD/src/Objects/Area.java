package Objects;

import Logic.AreaUtils;

public class Area extends Room{
	private Room[] area = new Room[4];
	
	public Area(int index, String name, String desc, int n, int s, int e, int w) {
		super(index, name, desc, n, s, e, w);
		AreaUtils.readCSV(this.area);
	}

	public Room[] getArea() {
		return area;
	}

}
