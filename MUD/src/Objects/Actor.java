package Objects;

import java.util.concurrent.ThreadLocalRandom;

public class Actor {
	private String name;
	private int hpmax, mpmax, hp, mp;
	private int aSTR, aAGL, aINT, aLCK;
	private int mSTR, mAGL, mINT, mLCK;
	private int minATK, maxATK;
	private String[] actions = new String[2];
	private Skill[] skills = new Skill[5];
	
	
public Actor(String name, int aSTR, int aAGL, int aINT, int aLCK, Skill[] skills) {
		this.name = name;
		this.aSTR = aSTR;
		this.aAGL = aAGL;
		this.aINT = aINT;
		this.aLCK = aLCK;
		
		this.mSTR = (this.aSTR/2)-5;
		this.mAGL = (this.aAGL/2)-5;
		this.mINT = (this.aINT/2)-5;
		this.mLCK = (this.aLCK/2)-5;
		
		this.hpmax = (10*(this.mSTR+1))+mAGL+20;
		this.mpmax = (10*(this.mINT+1))+mLCK+10;
		
		this.hp = this.hpmax;
		this.mp = this.mpmax;
		
		this.minATK = mSTR + mAGL;
		if(this.minATK <= 0) this.minATK = 1;
		this.maxATK = mSTR + mAGL + 4;
		if(this.maxATK <= 0) this.maxATK = this.minATK + 4;
		
		this.actions[0] = "Attack";
		this.actions[1] = "Use Skills";
		
		this.skills = skills;
	}

	//--------------------GETTERS AND SETTERS-------------------------
	public String[] getActions() {
		return actions;
	}
	
	public void setActions(String[] actions) {
		this.actions = actions;
	}	
	
	public String getName() {
		return name;
	}

	public int getMinATK() {
		return minATK;
	}

	public void setMinATK(int minATK) {
		this.minATK = minATK;
	}

	public int getMaxATK() {
		return maxATK;
	}

	public void setMaxATK(int maxATK) {
		this.maxATK = maxATK;
	}

	public int getHpmax() {
		return hpmax;
	}

	public void setHpmax(int hpmax) {
		this.hpmax = hpmax;
	}

	public int getMpmax() {
		return mpmax;
	}

	public void setMpmax(int mpmax) {
		this.mpmax = mpmax;
	}

	public int getHp() {
		return hp;
	}

	public void setHp(int hp) {
		this.hp = hp;
	}

	public int getMp() {
		return mp;
	}

	public void setMp(int mp) {
		this.mp = mp;
	}

	public int getaSTR() {
		return aSTR;
	}

	public void setaSTR(int aSTR) {
		this.aSTR = aSTR;
	}

	public int getaAGL() {
		return aAGL;
	}

	public void setaAGL(int aAGL) {
		this.aAGL = aAGL;
	}

	public int getaINT() {
		return aINT;
	}

	public void setaINT(int aINT) {
		this.aINT = aINT;
	}

	public int getaLCK() {
		return aLCK;
	}

	public void setaLCK(int aLCK) {
		this.aLCK = aLCK;
	}

	public int getmSTR() {
		return mSTR;
	}

	public void setmSTR(int mSTR) {
		this.mSTR = mSTR;
	}

	public int getmAGL() {
		return mAGL;
	}

	public void setmAGL(int mAGL) {
		this.mAGL = mAGL;
	}

	public int getmINT() {
		return mINT;
	}

	public void setmINT(int mINT) {
		this.mINT = mINT;
	}

	public int getmLCK() {
		return mLCK;
	}

	public void setmLCK(int mLCK) {
		this.mLCK = mLCK;
	}

	public void setName(String name) {
		this.name = name;
	}
//------------------METHODS--------------------------------
	public void takeDamage(int dmg) {
		this.hp -= dmg;
		if(this.hp < 0) this.hp = 0;
	}
	
	public int genDmg() {
		return ThreadLocalRandom.current().nextInt(minATK, maxATK);
	}

}
