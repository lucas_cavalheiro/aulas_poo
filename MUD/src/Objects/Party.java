package Objects;

public class Party {
	private int curr = 0, memCt = 0;
	private Actor[] members = new Actor[4];

	//GETTERS AND SETTERS
	public Actor[] getMembers() {
		return members;
	}

	public void setMembers(Actor[] members) {
		this.members = members;
	}

	public int getCurr() {
		return curr;
	}

	public void setCurr(int curr) {
		this.curr = curr;
	}
	//METHODS
	public void checkMove(String com, Area area) {
		switch(com) {
			case "n": northMover(com, area); break;
			case "s": southMover(com, area); break;
			case "e": eastMover(com, area); break;
			case "w": westMover(com, area); break;
		}
	}
	
	public void northMover(String com, Area area) {
		if(area.getArea()[curr].getN() != -1) {
			curr = area.getArea()[area.getArea()[curr].getN()].getIndex();
			System.out.println("You moved to the north now you're at " + area.getArea()[curr].getName());
		} else System.out.println("You can't move there");
	}
	
	public void southMover(String com, Area area) {
		if(area.getArea()[curr].getS() != -1) {
			curr = area.getArea()[area.getArea()[curr].getS()].getIndex();
			System.out.println("You moved to the south now you're at " + area.getArea()[curr].getName());
		} else System.out.println("You can't move there");
	}
	
	public void eastMover(String com, Area area) {
		if(area.getArea()[curr].getE() != -1) {
			curr = area.getArea()[area.getArea()[curr].getE()].getIndex();
			System.out.println("You moved to the east now you're at " + area.getArea()[curr].getName());
		} else System.out.println("You can't move there");
	}
	
	public void westMover(String com, Area area) {
		if(area.getArea()[curr].getW() != -1) {
			curr = area.getArea()[area.getArea()[curr].getW()].getIndex();
			System.out.println("You moved to the west now you're at " + area.getArea()[curr].getName());
		} else System.out.println("You can't move there");
	}
	
	public void addMember(Actor actor) {
		if(memCt == this.members.length-1) {
			System.out.println("Party is full!");
		}
		
		for(int i = 0; i <= this.members.length-1;i++) {
			if(this.members[i] == null) {
				this.members[i] = actor;
				memCt += 1;
			}
				
		}
	}
	
}
