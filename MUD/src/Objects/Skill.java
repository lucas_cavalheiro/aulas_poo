package Objects;

public class Skill {
	private String name;
	private int mag, cost;
//--------------------CONSTRUCTORS---------------------
	public Skill(String name, int mag, int cost) {
		this.name = name;
		this.mag = mag;
		this.cost = cost;
	}
//-------------------GETTERS AND SETTERS------------------	
	public String getName() {
		return name;
	}

	public int getMag() {
		return mag;
	}

	public int getCost() {
		return cost;
	}
//---------------------METHODS---------------------------
	
}
