package Logic;

import java.util.Scanner;
import Objects.*;

public abstract class Game {
	static String com;
	static boolean end = false;
	static Scanner sc = new Scanner(System.in);
	
	public static void main(String[] args) { //MAIN LOOP
		System.out.println("Welcome to the MUD");
		Area castle = new Area(0,"Castle","A dark castle.",-1,-1,-1,-1);
		
		Skill[] skillset = new Skill[3];
		skillset[0] = new Skill("Fire", 25, 5);
		skillset[1] = new Skill("Ice", 5, 1);
		skillset[2] = new Skill("Thunder", 50, 10);
		
		Actor hero = new Actor("Juberto",6,12,12,12,skillset);
		Party party = new Party();
		party.addMember(hero);
		System.out.println(Color.GREEN + party.getMembers()[0].getName() + Color.RESET + " joins the party!");
		System.out.println("You have entered " 
							+ Color.BLUE_BRIGHT + castle.getName() + " " 
							+ Color.YELLOW + castle.getDescription() 
							+ Color.RESET);
		
		Actor foe = new Actor("Goblin",16,12,12,12,null);
		
		boolean battle = true;
		
		System.out.println(Color.RED_BOLD_BRIGHT + "A " + foe.getName() + " attacks!" + Color.RESET);
		while (battle) {
			System.out.println("Choose action: ");
			int index = 1;
			for(String action: party.getMembers()[0].getActions()) {
				System.out.println(index + " - " + action);
				index++;
			}
			com = sc.next().toLowerCase();
			int dmg = 0;
			
			switch(com) {
				case "1":  dmg = party.getMembers()[0].genDmg(); foe.takeDamage(dmg); break;
				default: System.out.println("Invalid!"); continue;
			}
			
			System.out.println("You hit for: " + dmg + " foe's hp is " + foe.getHp() + " / " + foe.getHpmax());
			
			if(foe.getHp() <= 0) {
				System.out.println("A winner is you!"); 
				battle = false;
				break;
			}
			
			dmg = foe.genDmg();
			party.getMembers()[0].takeDamage(dmg);
			
			System.out.println(foe.getName() + " hits for: " + dmg + " your hp is " + party.getMembers()[0].getHp() + " / " + party.getMembers()[0].getHpmax());
			
			if(party.getMembers()[0].getHp() <= 0) {
				System.out.println("A loser is you!"); 
				battle = false;
			}
		}
		
		
		do {
			System.out.println("Name a command or type 'help' to display command list:");
			com = sc.next().toLowerCase();
			
			switch(com){
				case "n":  party.checkMove(com, castle); break;
				case "s":  party.checkMove(com, castle); break;
				case "e":  party.checkMove(com, castle); break;
				case "w":  party.checkMove(com, castle); break;
				case "help": MenuUtils.dHelp(); break;
				case "look": MenuUtils.look(castle, party.getCurr()); break;
				case "scout":MenuUtils.scout(castle, party.getCurr()); break;
				case "exit": end = true; break;
				default: ComException.handleComException(com);
			}
			
		} while (!end);
		
		System.out.println("Bye bye!");
	}
	
	public static void printStats(Actor actor) {
		System.out.println(actor.getaSTR());
		System.out.println(actor.getaAGL());
		System.out.println(actor.getaINT());
		System.out.println(actor.getaLCK());
		System.out.println();
		System.out.println(actor.getmSTR());
		System.out.println(actor.getmAGL());
		System.out.println(actor.getmINT());
		System.out.println(actor.getmLCK());
		System.out.println();
		System.out.println(actor.getHp() + " / " 
							+ actor.getHpmax());
		System.out.println(actor.getMp() + " / " 
							+ actor.getMpmax());
		System.out.println();
		System.out.println(actor.getMinATK());
		System.out.println(actor.getMaxATK());
		System.out.println();
		System.out.println(actor.genDmg());
		System.out.println(actor.genDmg());
		System.out.println(actor.genDmg());
		System.out.println(actor.genDmg());
		System.out.println(actor.genDmg());
		System.out.println(actor.genDmg());
		System.out.println(actor.genDmg());
	}

}
