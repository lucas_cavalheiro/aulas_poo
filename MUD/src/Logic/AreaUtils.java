package Logic;

import java.io.File;
import java.io.IOException;
import java.util.Scanner;

import Objects.Room;

public abstract class AreaUtils {
	public static void readCSV(Room[] area) {
		  File file = new File("src/castle.csv");
		  Scanner sc = null;
		  try {
			  sc = new Scanner(file);
		  
			   while(sc.hasNextLine()){
				   String str = sc.nextLine();
				   parseAndCreate(str, area);
			   }
		  } catch (IOException  exp) {
		   exp.printStackTrace();
		  }
		  sc.close();
	}
	
	public static void parseAndCreate(String str, Room[] area) {
		String index, name, desc, n, s, e, w;
		  Scanner sc = new Scanner(str);
		  sc.useDelimiter(";");
		  while(sc.hasNext()){
		   index = sc.next();
		   name = sc.next();
		   desc = sc.next();
		   n = sc.next();
		   s = sc.next();
		   e = sc.next();
		   w = sc.next();
		   area[Integer.parseInt(index)] = new Room(Integer.parseInt(index), name, desc, 
				   							Integer.parseInt(n), Integer.parseInt(s), 
				   							Integer.parseInt(e), Integer.parseInt(w));
		  }
		  sc.close();
	}
}
