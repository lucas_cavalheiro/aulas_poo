package Logic;

import Objects.Area;

public abstract class MenuUtils {
	public static void dHelp() {
		System.out.println("---------COMMAND LIST--------");
		System.out.println("n = North | s = South | e = East | w = West");
		System.out.println("Type 'look' to look around");
		System.out.println("Type 'scout' to see obvious available paths");
		System.out.println("Type 'exit' to exit the game");
		System.out.println("Type 'help' to display this command list");
		System.out.println("------------------------------");
	}
	
	public static void look(Area area, int curr) {
		System.out.println(area.getArea()[curr].getDescription());
	}

	public static void scout(Area area, int curr) {
		boolean canMove = false;
		System.out.print("I can move...");
		if(area.getArea()[curr].getN() != -1) System.out.print(Color.BLUE_BRIGHT + " north " + Color.RESET); canMove = true;
		if(area.getArea()[curr].getS() != -1) System.out.print(Color.BLUE_BRIGHT + " south " + Color.RESET); canMove = true;
		if(area.getArea()[curr].getE() != -1) System.out.print(Color.BLUE_BRIGHT + " east " + Color.RESET); canMove = true;
		if(area.getArea()[curr].getW() != -1) System.out.print(Color.BLUE_BRIGHT + " west " + Color.RESET); canMove = true;
		if(!canMove) System.out.println(Color.RED + " nowhere!" + Color.RESET);
		System.out.println();
	}
}
