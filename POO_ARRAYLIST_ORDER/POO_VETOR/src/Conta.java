
public abstract class Conta implements Comparable<Conta>, Imprimivel {

    public final static double TAXA = 0.05;
    
    //Atributos de inst�ncia
    private int numero;
    protected double saldo;
    private boolean status;
    
    //M�todos de acesso
    public int getNumero() {
        return numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }

    public double getSaldo() {
        return saldo;
    }

    public void setSaldo(double saldo) {
        this.saldo = saldo;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    //Construtor
    public Conta(int numero, double saldo) {
        this.numero = numero;
        this.saldo = saldo;
        this.status = true;
    }

    public void sacar(double valor) throws SIException {
        saldo -= valor;
    }
    
    public void depositar(double valor){
        saldo += valor;
    }

	@Override
	public int compareTo(Conta o) {
		if (this.saldo > o.getSaldo()) { 
			return -1; 
		} 
		if (this.saldo < o.getSaldo()) { 
			return 1;
		} 
		return 0; 

	}
	
	public abstract void print();
    
}
