
public class Poupanca extends Conta {
	
	private double rentabilidade;

	public Poupanca(int numero, double saldo) {
		super(numero, saldo);
		// TODO Auto-generated constructor stub
	}

	public double getRentabilidade() {
		return rentabilidade;
	}

	public void setRentabilidade(double rentabilidade) {
		this.rentabilidade = rentabilidade;
	}
	
	@Override
	public void depositar(double valor){
        saldo += valor;
        setSaldo(getSaldo() + valor);
        setRentabilidade(getRentabilidade() + valor * TAXA);
    }
	
	@Override
	public void print() {
		System.out.println(getNumero() + "\t" +
				   getSaldo() + "\t" ); 
	}
}

/*
 * Interface:
 * 		atributos
 * 		sem instancia
 * 		sem metodo concreto
 * 		só metodos abstratos
 * 
 * Classe abstrata:
 * 		atributos
 * 		metodos concretos
 * 		metodo abstrato
 * 		sem instancia
 * 
*/