
public class SIException extends Exception {
	private double saldo;
	private int numero;
	
	public SIException(double saldo, int numero) {
		super ("Saldo insuficiente!");
		this.saldo = saldo;
		this.numero = numero;
	}
	
	double getSaldo() {
		return this.saldo;
	}
	
	int getNumero() {
		return this.numero;
	}
	
	
	
}
