
public class Corrente extends Conta {

	private double limite;
	
	public double getLimite() {
		return limite;
	}

	public void setLimite(double limite) {
		this.limite = limite;
	}

	public Corrente(int numero,
			        double saldo,
			        double limite) {
		super(numero, saldo);
		this.limite = limite;
	}
	
	@Override
	public void sacar(double valor) throws SIException {
		if (valor < getSaldo()) {
			setSaldo(getSaldo() - valor);
		}else if (valor < limite) {
			limite = limite - valor;
		} else {
			SIException e;
			e = new SIException(getSaldo(), getNumero());
			throw e;
		}
	}
	
	@Override
	public void print() {
		System.out.println(getNumero() + "\t" +
				   getSaldo() + "\t" +
				   getLimite()); 
	}
	
}
