import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class APP {
    
    //vari�vel comum
    static int index = 0;
    
    //Lista de contas
    static ArrayList<Conta> lista = new ArrayList<>();
    
    static Scanner tecla = new Scanner(System.in);

    public static void main(String[] args) {
        int op;
        do {                
            System.out.println("*** MENU PRINCIPAL ***");
            System.out.println("1-Incluir Conta Corrente");
            
            System.out.println("2-Excluir Conta Corrente");
            System.out.println("3-Sacar");
            System.out.println("4-Depositar");
            System.out.println("5-Listar Contas em Ordem Crescente de Saldo");
            System.out.println("6-Sair");
            System.out.println("Digite sua op��o: ");
            op = tecla.nextInt(); 
            switch(op){
                case 1: incluirContaCorrente(); break;
                case 2: excluirConta(); break;
                case 3: sacarValor(); break;
                case 4: depositarValor(); break;    
                case 5: listarContas(); break;
                case 6: break;
            }
        } while (op!=6);       
    }
    
    public static void excluirConta() {
    	//Entrada
        System.out.println("Digite o n�mero da conta:");
        int num = tecla.nextInt();
        
        //Procurar a conta na lista para exclus�o
        for (Conta conta : lista) {
			if (conta.getNumero() == num) {
				lista.remove(conta);
				break;
			}
		}
    }
    
    public static void incluirContaCorrente(){
        //Entrada
        System.out.println("Digite o n�mero da conta:");
        int num = tecla.nextInt();
        System.out.println("Digite o saldo da conta:");
        double saldo = tecla.nextDouble();
        System.out.println("Digite o limite da conta:");
        double limite = tecla.nextDouble();
        //Criar o objeto e inserir na lista
        lista.add(new Corrente(num, saldo, limite));
        System.out.println("Conta cadastrada com sucesso!");
    }
    
    public static void sacarValor(){
        //Entrada
        System.out.println("Digite o n�mero da conta:");
        int num = tecla.nextInt();
        System.out.println("Digite o valor do saque:");
        double valor = tecla.nextDouble();
        
        //Procurar a conta na lista para efetuar o saque
        for (Conta conta : lista) {
			if (conta.getNumero() == num) {
				conta.sacar(valor);
				break;
			}
		}
    }
    
    public static void depositarValor(){
        //Entrada
        System.out.println("Digite o n�mero da conta:");
        int num = tecla.nextInt();
        System.out.println("Digite o valor do dep�sito:");
        double valor = tecla.nextDouble();
        
        //Procurar a conta na lista para efetuar o dep�sito
        for (Conta conta : lista) {
			if (conta.getNumero() == num) {
				conta.depositar(valor);
				break;
			}
		}
    }
    
    public static void ordenarLista() {
    	Collections.sort(lista);
    }
    
    //M�todo polim�rfico
    public static void imprimir(Conta p) {
    	p.print();
    }
    
    public static void listarContas(){
        double total = 0;
        System.out.println("=======================");
        System.out.println("CONTA:	SALDO:	LIMITE:");
        System.out.println("=======================");
        
        ordenarLista();
        
        for (Conta conta : lista) {
			
        	if (!conta.equals(null)) {
        		
        		imprimir(conta);
        		
//				if (conta instanceof Corrente) {
//					Corrente c = (Corrente) conta;
//					System.out.println(c.getNumero() + "\t" +
//									   c.getSaldo() + "\t" +
//									   c.getLimite()); 
//				}
//				
//				if (conta instanceof Poupanca) {
//					Poupanca p = (Poupanca) conta;
//					System.out.println(p.getNumero() + "\t" +
//									   p.getSaldo() + "\t" +
//									   p.getRentabilidade());	
//				}
				
				
			total += conta.getSaldo();
			}
		}
        System.out.println("Total:....." + total);
        System.out.println("=======================");
    }
}