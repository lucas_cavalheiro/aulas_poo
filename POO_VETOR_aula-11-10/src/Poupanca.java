
public class Poupanca extends Conta {
	
	public final static double TAXA = 0.05;
	
	private double rentabilidade;
	
	public Poupanca(int numero, double saldo) {
		super(numero, saldo);
		this.rentabilidade = 0;
	}

	public double getRentabilidade() {
		return rentabilidade;
	}

	public void setRentabilidade(double rentabilidade) {
		this.rentabilidade = rentabilidade;
	}
	
	@Override
	public void depositar(double valor) {
		setSaldo(getSaldo()+ valor);
		setRentabilidade(getRentabilidade() + valor * TAXA);
	}
	
	public void print() {
		System.out.println(getNumero() + "\t" +
				           getSaldo() + "\t" +
				           getRentabilidade() + "\t" +
				           isStatus());
	}

}