package utilitarios;

public abstract class Utils {
	//--------------------------CRUD--------------------------------
	public static Integer[] create(Integer[] arr) {
		System.out.println("Define number: ");
		int num = APP.sc.nextInt();
		boolean found = false;
		for(int i = 0; i <= arr.length-1; i++) {
			if(arr[i] == null) {
				arr[i] = num;
				found = true;
				break;
			}
		}
		if(!found) System.out.println("No space!");
		return arr;
	}
	
	public static void read(Integer[] arr) {
		for (int i = 0; i <= arr.length-1; i++) {
			System.out.println("Index: " 
								+ Color.BLUE_BOLD_BRIGHT + i + " " + Color.RESET
								+ "Number: " + Color.YELLOW_BOLD_BRIGHT + arr[i] + Color.RESET);
		}
	}
	
	public static Integer[] update(Integer[] arr) {
		read(arr);
		System.out.println("Define index:");
		int index = APP.sc.nextInt();
		System.out.println("Define number: ");
		int num = APP.sc.nextInt();
		boolean found = false;
		for(int i = 0; i <= arr.length-1; i++) {
			if(arr[i] != null && index == i) {
				arr[i] = num;
				found = true;
				break;
			}
		}
		if(!found) System.out.println("Not found!");
		return arr;
	}
	
	public static Integer[] delete(Integer[] arr) {
		read(arr);
		System.out.println("Define index:");
		int index = APP.sc.nextInt();
		boolean found = false;
		for(int i = 0; i <= arr.length-1; i++) {
			if(arr[i] != null && index == i) {
				arr[i] = null;
				found = true;
				break;
			}
		}
		if(!found) System.out.println("Not found!");
		return arr;
	}
	//----------------------COMPACT-----------------------------------
	public static Integer[] compact(Integer[] arr) {
		Integer aux;
		for(int i = 0; i <= arr.length-1; i++) {
			if(arr[i] == null) {
				for(int j = 0; j <= arr.length-1; j++) {
					if(arr[j] != null) {
						aux = arr[j];
						arr[j] = arr[i];
						arr[i] = aux;
					}
				}
			}
		}
		return arr;
	}
	//-----------------------SORT------------------------------------
	public static Integer[] selectionSort(Integer[] arr) {
		Integer aux;
		for(int i = 0; i <= arr.length-1; i++) {
			for(int j = 0; j <= arr.length-1; j++) {
				if(arr[i] != null && arr[j] != null && arr[i] > arr[j]) {
					aux = arr[i];
					arr[i] = arr[j];
					arr[j] = aux;
				}
			}
			
		}
		return arr;
	}
	
	public static Integer[] bubbleSort(Integer[] arr) {
		Integer aux;
		for(int i = 0; i <= arr.length-1; i++) {
			for(int j = 1; j <= arr.length-1-i; j++) {
				if(arr[j] != null && arr[j-1] != null &&arr[j-1] > arr[j]) {
					aux = arr[j-1];
					arr[j-1] = arr[j];
					arr[j] = aux;
				}
			}
			
		}
		return arr;
	}
	
	public static Integer[] insertionSort(Integer[] arr) {
		for(int i = 1; i < arr.length; i++) {
			Integer key = arr[i];
			int j = i-1;
			if(arr[j] != null && key != null) {
				while (j > -1 && arr[j] > key) {
					arr[j+1] = arr[j];
					j--;
					if(arr[j] == null) break;
				}
				arr[j+1] = key;
			}
			
		}
		
		return arr;
	}
	
}
