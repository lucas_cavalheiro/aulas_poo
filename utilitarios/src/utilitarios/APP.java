package utilitarios;

import java.util.Scanner;
import java.util.concurrent.ThreadLocalRandom;

public class APP {
	static int opt = 0;
	static boolean end = false;
	static Scanner sc = new Scanner(System.in);
	
	public static void main(String[] args) {
		System.out.println("Defina tamanho da array:");
		int tam = sc.nextInt();
		Integer[] arr = new Integer[tam];
		for (int i = 0; i <= arr.length-1; i++) {
			arr[i] = (i + ThreadLocalRandom.current().nextInt(1, arr.length+i));
		}
		
		do {
			System.out.println("Selecione uma opção: ");
			System.out.println("1 - Create");
			System.out.println("2 - Read");
			System.out.println("3 - Update");
			System.out.println("4 - Delete");
			System.out.println("5 - Sort");
			System.out.println("6 - Compact");
			System.out.println("0 - Sair");
			opt = sc.nextInt();
			
			switch(opt) {
				case 1: arr = Utils.create(arr); break;
				case 2: Utils.read(arr); break;
				case 3: arr = Utils.update(arr); break;
				case 4: arr = Utils.delete(arr); break;
				case 5: sortHandler(arr); break;
				case 6: arr = Utils.compact(arr); break;
				case 0: end = true; break;
				default: System.out.println("Inválido!"); break;
			}
		} while (!end);

	}
	
	public static void sortHandler(Integer[] arr) {
		int opt = 0;
		boolean back = false;
		do {
			System.out.println("Selecione método de Sort: ");
			System.out.println("1 - Simple Sort");
			System.out.println("2 - Bubble Sort");
			System.out.println("3 - Insertion Sort");
			System.out.println("0 - Sair");
			opt = sc.nextInt();
			
			switch(opt) {
				case 1: arr = Utils.selectionSort(arr); back = true; break;
				case 2: arr = Utils.bubbleSort(arr); back = true; break;
				case 3: arr = Utils.insertionSort(arr); back = true; break;
				case 0: back = true; break;
				default: System.out.println("Inválido!"); break;
			}
		} while (!back);
	}
	
	

}
