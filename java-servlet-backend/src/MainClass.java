import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class MainClass extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	 static ArrayList<Carro> carros = new ArrayList<>();
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		RequestDispatcher view = req.getRequestDispatcher("index.html");
		view.forward(req, resp);
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String numero = req.getParameter("numero");
        String fabricante = req.getParameter("fabricante");
        String modelo = req.getParameter("modelo");
        String ano = req.getParameter("ano");
        String categoria = req.getParameter("categoria");
        String cdplayer = req.getParameter("cdplayer");
        String dvdplayer = req.getParameter("dvdplayer");
        String disponivel = req.getParameter("disponivel");
        resp.setContentType("text/html");
        
        Carro carro = new Carro(numero, fabricante, modelo, ano, categoria, cdplayer, dvdplayer, disponivel);
        carros.add(carro);
        System.out.println(carros.toString());
        
        PrintWriter out = resp.getWriter();
        out.println("<html><head><link rel=\"stylesheet\" href=\"index.css\"></head><body>");
        out.println("<h1>Carro cadastrado com successo:</h1>");
        out.println("<p>");
        out.printf("Número: %s <br/> Fabricante: %s <br/> Modelo: %s <br/> Ano: %s <br/> Categoria: %s <br/> CD Player: %s <br/> DVD Player: %s <br/> Disponível: %s <br/>"
        		, carro.getNumero(), carro.getFabricante(), carro.getModelo()
        		, carro.getAno(), carro.getCategoria(), carro.getCdplayer()
        		, carro.getDvdplayer(), carro.getDisponivel());
        out.println("<form action=editor method=get><input class=\"btn btn-ok\" type=\"submit\" value=\"Voltar\" /></form>");
        out.println("</p>");
        out.println("</body></html>");
        out.close();
    }
	

}
