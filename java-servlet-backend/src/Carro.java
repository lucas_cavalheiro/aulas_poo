
public class Carro {
	private String numero;
	private String fabricante;
	private String modelo;
	private String ano;
	private String categoria;
	private String cdplayer;
	private String dvdplayer;
	private String disponivel;
	
	public Carro(String numero, String fabricante, String modelo, String ano, String categoria, String cdplayer, String dvdplayer, String disponivel) {
		this.numero = numero == "" ? "Não informado" : numero;
		this.fabricante = fabricante == "" ? "Não informado" : fabricante;
		this.modelo = modelo == "" ? "Não informado" : modelo;
		this.ano = ano == "" ? "Não informado" : ano;
		this.categoria = categoria == "" ? "Não informado" : categoria;
		this.cdplayer = cdplayer == null ? "Não" : cdplayer;
		this.dvdplayer = dvdplayer == null ? "Não" : dvdplayer;
		this.disponivel = disponivel == null ? "Não" : disponivel;
	}
	
	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public String getFabricante() {
		return fabricante;
	}

	public void setFabricante(String fabricante) {
		this.fabricante = fabricante;
	}

	public String getModelo() {
		return modelo;
	}

	public void setModelo(String modelo) {
		this.modelo = modelo;
	}

	public String getAno() {
		return ano;
	}

	public void setAno(String ano) {
		this.ano = ano;
	}

	public String getCategoria() {
		return categoria;
	}

	public void setCategoria(String categoria) {
		this.categoria = categoria;
	}

	public String getCdplayer() {
		return cdplayer;
	}

	public void setCdplayer(String cdplayer) {
		this.cdplayer = cdplayer;
	}

	public String getDvdplayer() {
		return dvdplayer;
	}

	public void setDvdplayer(String dvdplayer) {
		this.dvdplayer = dvdplayer;
	}

	public String getDisponivel() {
		return disponivel;
	}

	public void setDisponivel(String disponivel) {
		this.disponivel = disponivel;
	}
	
}
